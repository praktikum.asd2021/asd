/**
	PRAKTIKUM LISTLINEAR
	DEFENISI
	1. List Kosong <-> First(L) bernilai Nil
	2. P merupakan pointer ke ElementList
	3. Dikatakan Element terakhir apabila Next(P) bernilai Nil
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

typedef int infotype;
typedef struct ElmtList* address;
struct ElmtList {
	infotype 	...(b)...;
	address 	next;
};

struct List{
	address		first;
};

//SELEKTOR 
#define First(L) (L).first
#define Next(P) (P)->next
#define Info(P) (P)->info

//KONSTANTA
#define Nil NULL
#define Infinity 99999

 

bool isListEmpty (List L){
	/* Mengirim true jika list kosong */
	return ...(c)...;
}

void createList (List *L){
	/*	I.S. sembarang
		F.S. Terbentuk list kosong 
	*/
	...(d)...

}

address getLast(List L){
    //Mengembalikan alamat element terakhir
    address last = L.first;
    while (last->next != ....)
    {
        last = ....;
    }
    return last;
}

address alokasi (infotype X){
	/*	Mengirimkan address hasil alokasi sebuah elemen
		Jika alokasi berhasil, maka address tidak Nil, dan misalnya menghasilkan P, maka
		Info(P) = X, Next(P) = Nil
		Jika alokasi gagal, mengirimkan Nil
	*/
	address P = new ElmtList;
	if(...(f)...)
		return Nil;

	...(g)... = X;
	Next(P) = ...(h)...;
	return ...(i)...;
	
}

void dealokasi (address *P){
	/*	I.S. P terdefinisi
		F.S. P dikembalikan ke sistem
		Melakukan dealokasi/pengembalian address P
	*/
	delete[] *P;
}

void insertFirst (List *L, infotype X){
	/*	I.S. L mungkin kosong
		F.S. X ditambahkan sebagai elemen pertama L
		Proses : Melakukan alokasi sebuah elemen dan menambahkan elemen pertama dengan
		nilai X jika alokasi berhasil.
		Jika alokasi gagal: I.S.= F.S.
	*/
	address P = ...(j)...;
	if(P!=Nil){
		...(k)...=First(*L);
		First(*L) = ...(l)...;
	}
}

void insertAfter (List *L, infotype X, address Prec){
	/*	I.S. Prec pastilah elemen list dan bukan elemen terakhir,
		P sudah dialokasi
		F.S. Insert P sebagai elemen sesudah elemen beralamat Prec
	*/
    address P = ...(j)...;
	if(P!=Nil){
        Next(P)=...(m)...;
        ...(n)...=P;
    }
}

void insertLast (List *L, infotype X){
	/*	I.S. L mungkin kosong
		F.S. X ditambahkan sebagai elemen terakhir L
		Proses : Melakukan alokasi sebuah elemen dan menambahkan elemen list di akhir :
		elemen terakhir yang baru bernilai X jika alokasi berhasil.
		Jika alokasi gagal: I.S.= F.S.
	*/
	if(isListEmpty(*L))
	    .....; //masukkan diawal linked list
	else{
        address last = getLast(...);
		insertAfter(L, ...(t)..., ....);
	}
}

void deleteFirst (List *L, infotype *X){
	/*	I.S. List L tidak kosong
		F.S. Elemen pertama list dihapus : nilai info disimpan pada X
		dan alamat elemen pertama di-dealokasi
	*/
	address P = First(*L);
	*X = Info(P);
    ........; //mendefinisikan first yang baru
    ........; //dealokasi first yang lama
    
}

void deleteAfter (List *L, address *Pdel, address Prec){
	/*	I.S. List tidak kosong. Prec adalah anggota list L.
		F.S. Menghapus Next(Prec) : Pdel adalah alamat elemen list yang dihapus
	*/
	*Pdel = Next(Prec);
	if(*Pdel!=Nil) {
		...(3)...;
	}
	dealokasi(Pdel);
}

void deleteLast(List *L, infotype *X){
	/*	I.S. list tidak kosong
		F.S. Elemen terakhir list dihapus : nilai info disimpan pada X
		dan alamat elemen terakhir di-dealokasi
	*/
	if(Next(...(4)...)==Nil){
		//one element only
		*X = Info(First(*L));
        .......; //dealokasi element akhir
		createList(L);
	}else{
		address last = First(*L);
        address before_last = Nil;
        while (Next(last) != NULL){
            prev = .......;
            before_last = .......;
        }
		*X = Info(last);
		...(7)...;
        .........; //dealokasi last element
	}
}

void printInfo (List L){
	/*	I.S. List mungkin kosong
		F.S. Jika list tidak kosong,
		Semua info yg disimpan pada elemen list diprint dengan format [elemen-1, elemen-2, elemen-3, ...]
		Jika list kosong, hanya menuliskan "[]"
	*/
	cout<<...(16)...;
	if(!isListEmpty(L)){
		address P = First(L);
		while(...(17)...){ //mencetak seluruh element linked list
            
		}
	}
	cout<<...(19)...<<endl;
}
